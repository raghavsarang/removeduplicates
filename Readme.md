Remove Duplicates with 3 different implementations

1.BruteForce method with additional memory by creating new array 

2.Improved inplace removal of duplicates but requires sorting overhead. 
    -- Using java inbuilt sort has a performance O(nlog(n)) + O(n)
3.Using java collections to remove duplicates and retain the order
    -- LinkedHashSet give us the facility to use hash keys along with insertion order
    


