/**
 * DeDup utility class
 */
package org.sei.dedup;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author raghavendrasarangapurkar
 * 
 * @version 1.0
 *
 */
public final class DeDup {

	// static int[] randomIntegers =
	// {1,2,34,34,25,1,45,3,26,85,4,34,86,25,43,2,1,
	// 10000,11,16,19,1,18,4,9,3, 20,17,8,15,6,2,5,
	// 10,14,12,13,7,8,9,1,2,15,12,18,10,
	// 14,20,17,16,3,6,19, 13,5,11,4,7,19,16,5,9,
	// 12,3,20,7,15,17,10,6,1,8,18,4,14,13,2,11};

	// static int[] randomIntegers = {1,2,34,34,25,1};

	public static void main(String arg[]) {
		int[] randomIntegers = { 1, 2, 34, 34, 25, 1, 45, 3, 26, 85, 4, 34, 86, 25, 43, 2, 1, 10000, 11, 16, 19, 1, 18,
				4, 9, 3, 20, 17, 8, 15, 6, 2, 5, 10, 14, 12, 13, 7, 8, 9, 1, 2, 15, 12, 18, 10, 14, 20, 17, 16, 3, 6,
				19, 13, 5, 11, 4, 7, 19, 16, 5, 9, 12, 3, 20, 7, 15, 17, 10, 6, 1, 8, 18, 4, 14, 13, 2, 11 };

		printNumbers(randomIntegers);
		printNumbers(removeDuplicates(randomIntegers));
		printNumbers(removeDuplicatesImprovedSpeed(randomIntegers));
		printNumbers(removeDuplicatesOrderRetained(randomIntegers));

	}

	/**
	 * Returns a new array without duplicates
	 * 
	 * @param inputArray
	 * @return array
	 * @throws NullPointerException,
	 *             IllegalArgumentException
	 */
	public synchronized final static int[] removeDuplicates(int[] inputArray)
			throws NullPointerException, IllegalArgumentException {

		checkArrForErrors(inputArray);

		/**
		 * Result array of the size equal to input array -- Covers inputArray
		 * with all unique elements
		 */
		int[] resultArray = new int[inputArray.length];
		boolean found = false;
		int resultArrayIndex = 0;
		/**
		 * An integer to track the max element in the new array
		 */
		int resultArrayCurrentLength = 0;

		int inputArrayLength = inputArray.length;

		/**
		 * For each element in inputArray check if already added to the new
		 * result array
		 * 
		 * Worst case complexity O(n2)
		 * 
		 * 
		 */
		for (int idx = 0; idx < inputArrayLength; idx++) {
			found = false;
			for (resultArrayIndex = 0; resultArrayIndex < resultArrayCurrentLength; resultArrayIndex++) {
				if (inputArray[idx] == resultArray[resultArrayIndex]) {
					found = true;
				}

			}
			if (!found) {
				resultArray[resultArrayCurrentLength++] = inputArray[idx];
			}
		}
		return resultArray;
	}

	/**
	 * 
	 * @param inputArray
	 * @return resultArray without duplicates in sorted order
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */
	public synchronized final static int[] removeDuplicatesImprovedSpeed(int[] inputArray)
			throws NullPointerException, IllegalArgumentException {
		checkArrForErrors(inputArray);
		/**
		 * Sort the array to get duplicates next to each other this reduces the
		 * O(n2) to O(nlog(n)) + O(n) time complexity
		 */
		Arrays.sort(inputArray); // Dual pivot algorithm internally used by java
									// sdk. Gives O(nlog(n)) performance

		int[] result = new int[inputArray.length];
		int k = inputArray[0], cnt = 0;
		result[cnt++] = k;

		for (int idx = 1; idx < inputArray.length; idx++) {
			if (k != inputArray[idx]) { // Compare if the current value is
										// already found
				k = inputArray[idx]; // Store the new value that is not a
										// duplicate
				result[cnt++] = k;
			}
		}

		return result;
	}

	/**
	 * 
	 * @param inputArray
	 * @return resultArray
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 */

	public synchronized final static int[] removeDuplicatesOrderRetained(int[] inputArray)
			throws NullPointerException, IllegalArgumentException {
		checkArrForErrors(inputArray);
		int[] result;
		Set<Integer> set = new LinkedHashSet<>();

		for (int idx = 0; idx < inputArray.length; idx++) {
			if (!set.contains(inputArray[idx]))
				set.add(inputArray[idx]);
		}

		result = new int[set.size()];
		int k = 0;
		Iterator<Integer> it = set.iterator();

		while (it.hasNext()) {
			result[k++] = it.next().intValue();
		}

		return result;
	}

	/**
	 * Print the input arr 
	 * @param arr
	 */
	private static void printNumbers(int[] arr) {
		for (int num : arr) {
			System.out.print(" " + num + " ");
		}
		System.out.println();
	}

	/**
	 * Check if array is null or empty
	 * @param arr
	 * @return
	 * @throws NullPointerException, IllegalArgumentException
	 */
	private static boolean checkArrForErrors(int[] arr) {
		if (arr == null)
			throw new NullPointerException("Array cannot be null");
		if (arr.length == 0)
			throw new IllegalArgumentException("Array cannot be empty");
		return false;
	}

}
