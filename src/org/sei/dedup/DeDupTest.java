/**
 * 
 */
package org.sei.dedup;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * @author raghavendrasarangapurkar
 * 
 * @version 1.0
 *
 */
public class DeDupTest {

 
 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	
		 
		 
		
	}
 
	
	/**
	 * Test for empty array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesEmptyArr() {
		int[] arr = {};
		try {
			DeDup.removeDuplicates(arr);
			fail("Exception Expected but data was processed"); // TODO
		} catch (IllegalArgumentException e) {

		}
	}
	
	/**
	 * Test for null array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesNullArr() {
		try {
			DeDup.removeDuplicates(null);
			fail("Exception Expected but data was processed"); // TODO
		} catch (NullPointerException e) {

		}
	}
	
	/**
	 * Test for large array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesLargeArr() {
		int[][] arr = generateArray(100000,true, false);
		 
		try {
			 assertArrayEquals(arr[1],  DeDup.removeDuplicates(arr[0]));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}
	
	/**
	 * Test method for {@link org.sei.dedup.DeDup#removeDuplicates(int[])} with all duplicate elements in 
	 * array
	 */
	@Test
	public final void testRemoveDuplicatesForAllDuplicateNumbers() {
		int[] arr = {1,1,1,1,1,1};
		int[] expected = {1,0,0,0,0,0};
		try {
			 assertArrayEquals(expected,  DeDup.removeDuplicates(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}

	/**
	 * Test for array with both negative and positive elements for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesWithBothNegativeAndPostiveElementsArr() {
		int[] arr = {1,-1,-2,3,-1,1};
		int[] expected = {1,-1,-2,3,0,0};
		try {
			 assertArrayEquals(expected,  DeDup.removeDuplicates(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}
	
	/**
	 * Test for empty array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesImprovedSpeedEmptyArr() {
		int[] arr = {};
		try {
			DeDup.removeDuplicatesImprovedSpeed(arr);
			fail("Exception Expected but data was processed"); // TODO
		} catch (IllegalArgumentException e) {

		}
	}
	
	/**
	 * Test for null array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesImprovedSpeedNullArr() {
		try {
			DeDup.removeDuplicatesImprovedSpeed(null);
			fail("Exception Expected but data was processed"); // TODO
		} catch (NullPointerException e) {

		}
	}
	
	/**
	 * Test for large array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesImprovedSpeedLargeArr() {
		int[][] arr = generateArray(10,true, false);
		 
		try {
			 Arrays.sort(arr[1]);
			 assertArrayEquals(arr[1],  DeDup.removeDuplicatesImprovedSpeed(arr[0]));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}
	
	/**
	 * Test method for {@link org.sei.dedup.DeDup#removeDuplicates(int[])} with all duplicate elements in 
	 * array
	 */
	@Test
	public final void testRemoveDuplicatesImprovedSpeedForAllDuplicateNumbers() {
		int[] arr = {1,1,1,1,1,1};
		int[] expected = {1,0,0,0,0,0};
		try {
			 assertArrayEquals(expected,  DeDup.removeDuplicatesImprovedSpeed(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}

	/**
	 * Test for array with both negative and positive elements for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesImprovedSpeedWithBothNegativeAndPostiveElementsArr() {
		int[] arr = {1,-1,-2,3,-1,1};
		int[] expected = {-2,-1,1,3,0,0};
		try {
			 
			 assertArrayEquals(expected,  DeDup.removeDuplicatesImprovedSpeed(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}
	
	
	/**
	 * Test for empty array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesOrderRetainedEmptyArr() {
		int[] arr = {};
		try {
			DeDup.removeDuplicatesOrderRetained(arr);
			fail("Exception Expected but data was processed"); // TODO
		} catch (IllegalArgumentException e) {

		}
	}
	
	/**
	 * Test for null array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesOrderRetainedNullArr() {
		try {
			DeDup.removeDuplicatesOrderRetained(null);
			fail("Exception Expected but data was processed"); // TODO
		} catch (NullPointerException e) {

		}
	}
	
	/**
	 * Test for large array for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesOrderRetainedLargeArr() {
		int[][] arr = generateArray(10,true, false);
		 
		try {
			 
			 assertArrayEquals(arr[2],  DeDup.removeDuplicatesOrderRetained(arr[0]));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}
	
	/**
	 * Test method for {@link org.sei.dedup.DeDup#removeDuplicates(int[])} with all duplicate elements in 
	 * array
	 */
	@Test
	public final void testRemoveDuplicatesOrderRetainedForAllDuplicateNumbers() {
		int[] arr = {1,1,1,1,1,1};
		int[] expected = {1};
		try {
			 assertArrayEquals(expected,  DeDup.removeDuplicatesOrderRetained(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}

	/**
	 * Test for array with both negative and positive elements for RemoveDuplicates method
	 */
	@Test
	public final void testRemoveDuplicatesOrderRetainedWithBothNegativeAndPostiveElementsArr() {
		int[] arr = {1,-1,-2,3,-1,1};
		int[] expected = {1,-1,-2,3};
		try {
			 
			 assertArrayEquals(expected,  DeDup.removeDuplicatesOrderRetained(arr));
			
		} catch (IllegalArgumentException e) {
			fail("Exception occurred");  
		}
	}

	
	
	private int[][] generateArray(int arraySize, boolean containsNegative, boolean avoidDuplicates){
		int[] withDuplicates = new int[arraySize];
		int[] withoutDuplicates = new int[arraySize];
		
		int[][] result = new int[3][];
		Random rand = new Random();
		Set<Integer> set =new LinkedHashSet<>();
		int isNeg = 0;
		int temp=0;
		for(int i=0,j=0;i<arraySize-1;){
			if(containsNegative){
				isNeg = rand.nextInt(2);
			}
			temp = rand.nextInt(65536);
			if(isNeg==1){
				temp = temp-32768;
			}
			 
			if (!set.contains(temp)) {
				set.add(temp);
				withoutDuplicates[j++] = temp;
				withDuplicates[i++] = temp;
			}else{
			
				if (!avoidDuplicates) {
					withDuplicates[i++] = temp;
				}  
			}
			 
			
		}
		result[0] = withDuplicates;
		result[1] = withoutDuplicates;
		if(!set.contains(0)){
			set.add(0); //Covers edge case where arraySize > set.size() and has 0's in it.
		}
		int[] reducedSizeArray = new int[set.size()];
		
		int k = 0;
		Iterator<Integer> it = set.iterator();

		while (it.hasNext()) {
			reducedSizeArray[k++] = it.next().intValue();
		}

		result[2] = reducedSizeArray;
		
		return result;
	}
	
	private int[] generateBooleanArray(int arraySize, boolean containsNegative){
		int[] arr = new int[arraySize];
		Random rand = new Random();
		int isNeg = 0;
		int temp=0;
		for(int i=0;i<arraySize;i++){
			if(containsNegative){
				isNeg = rand.nextInt(2);
			}
			temp = rand.nextInt(2);
			if(isNeg==1){
				temp = temp*-1;
			}
			
			arr[i] = temp;
		}
		
		return arr;
	}

}
